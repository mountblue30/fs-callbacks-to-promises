/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/


const fs = require("fs/promises");
const path = require("path");

function createDirectory(dirName){
    return fs.mkdir(path.join(__dirname, dirName));
}

function writeIntoFile(dirName, fileName, data) {
    return fs.writeFile(path.join(__dirname, dirName, `${fileName}`), JSON.stringify(data));

}

function deleteFile(dirName, fileName) {
    return fs.unlink(path.join(__dirname, dirName, `${fileName}`));

}

function deleteDirectory(dirName) {
    return fs.rmdir(path.join(__dirname, dirName));

}

function problemOne() {
    let dirName = "dummyDirectory";
    let dataContent = Array(100).fill(0).map((item) => {
        return {
            1: 100,
            2: 200,
            3: 300
        }
    })
    let files = Array(10).fill(0).map((element, index) => {
        return `${index}.json`;
    });
    
    createDirectory(dirName)
        .then(() => {
            return Promise.all(files.map((file) => {
                return writeIntoFile(dirName, file, dataContent);
            }))
        })
        .then(() => {
            console.log("Created files.");
            return Promise.all(files.map((file) => {
                return deleteFile(dirName, file);
            }))
        })
        .then(() => {
            console.log("Deleted files.");
            return deleteDirectory(dirName);
        })
        .then(() => {
            console.log("Deleted directory.");
        })
        .catch((err) => {
            console.log(err);
        })



}

module.exports = problemOne;


