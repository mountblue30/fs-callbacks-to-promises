/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/


const fs = require("fs/promises");
let path = require("path");




function writeIntoFile(fileName, data) {
    return fs.writeFile(path.join(__dirname, fileName), data);
}

function appendIntoFile(fileName,data){
    return fs.appendFile(path.join(__dirname, fileName), data);
}
function readFromFile(fileName){
    return fs.readFile(path.join(__dirname, fileName));
}

function processSentence(data){
    let sentences = data.toString().toLowerCase().split(".")
    let resultString = sentences.reduce((accu, sentence) => {
        accu += sentence + "\n";
        return accu;
    }, "")
    return resultString;
}

function collectingAllWords(data){
    return data.reduce((accu, data) => {
        accu = accu.concat(data.value.toString().split(" "));
        return accu;
    }, [])
}
function sortData(data){
    return collectingAllWords(data).sort((stringA, stringB) => {
        return stringA.localeCompare(stringB);
    });
}
function splitFileName(data){
    return data.toString().split("\n");
}
function deleteFile(fileName) {
    return fs.unlink(path.join(__dirname, fileName));

}


function problemTwo() {
    readFromFile("lipsum.txt")
        .then((data) => {
            return writeIntoFile("upperCase.txt", data.toString().toUpperCase());
        })
        .then(() => {
            return writeIntoFile("filenames.txt", "upperCase.txt");
        })
        .then(() => {
            return readFromFile("filenames.txt");
        })
        .then((data) => {
            return readFromFile(data.toString());
        })
        .then((data) => {
            return writeIntoFile("lowerCase.txt", processSentence(data));
        })
        .then(() => {
            return appendIntoFile("filenames.txt", `\nlowerCase.txt`);

        })
        .then(() => {
            return readFromFile("filenames.txt");
        })
        .then((data) => {
            let fileNames = splitFileName(data); 
            return Promise.allSettled(fileNames.map((file) => {
                return readFromFile(file);
            }))
        })
        .then((data) => {
            let sortedData = sortData(data);
            return writeIntoFile("sortedFile.txt",sortedData);
        })
        .then(() => {
            return appendIntoFile("filenames.txt", `\nsortedFile.txt`);
        })
        .then(() => {
            return readFromFile("filenames.txt");
        })
        .then((data) => {
            let fileNames = splitFileName(data);
            return Promise.allSettled(fileNames.map((file) => {
                return deleteFile(file);
            }))
        })
        .then(() => {
            console.log("Done");
        })
        

}


module.exports = problemTwo;
